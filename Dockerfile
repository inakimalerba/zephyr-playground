FROM debian:buster

# Speed up installs with eatmydata
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        eatmydata

###################
# ESP32 TOOLCHAIN #
###################

# https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/linux-setup.html
RUN eatmydata apt-get update && \
    eatmydata apt-get install -y --no-install-recommends \
        git \
        wget \
        flex \
        bison \
        gperf \
        python3 \
        python3-pip \
        python3-setuptools \
        cmake \
        ninja-build \
        ccache \
        libffi-dev \
        libssl-dev \
        dfu-util

# Python user installed packages
ENV PATH "${PATH}:${HOME}/.local/bin"
# Some scripts expect /usr/bin/python to be a thing
RUN ln -s /usr/bin/python3 /usr/bin/python

# Set up SDK - ESP-IDF
ENV IDF_TOOLS_PATH /opt/esp-idf-tools
ENV ESP_IDF_PATH /opt/esp-idf.git

# https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#get-started-get-esp-idf
# The branch is the latest release when writing this. Just to freeze it.
RUN git clone  \
    --recursive \
    --branch v4.0.2 \
    https://github.com/espressif/esp-idf.git \
    ${ESP_IDF_PATH}

# Set up Toolchain
RUN ${ESP_IDF_PATH}/install.sh

ENV ZEPHYR_TOOLCHAIN_VARIANT espressif
#ENV ESPRESSIF_TOOLCHAIN_PATH /opt/esp-idf-tools/tools/xtensa-esp32-elf/
# I guess this will change with the releases :shrug:
ENV ESPRESSIF_TOOLCHAIN_PATH ${IDF_TOOLS_PATH}/tools/xtensa-esp32-elf/esp-2020r3-8.4.0/xtensa-esp32-elf

###################
#     ZEPHYR      #
###################

RUN eatmydata apt-get update && \
    eatmydata apt-get install -y --no-install-recommends \
        git \
        cmake \
        ninja-build \
        gperf \
        ccache \
        dfu-util \
        device-tree-compiler \
        wget \
        python3-dev \
        python3-pip \
        python3-setuptools \
        python3-tk \
        python3-wheel \
        xz-utils \
        file \
        make \
        gcc \
        gcc-multilib \
        g++-multilib \
        libsdl2-dev \
        vim

# West is a helper used by zephyr to configure subrepos and stuff
RUN python3 -m pip install west

ENV WORKDIR /opt/zephyrproject
WORKDIR ${WORKDIR}

# Install Zephyr
# Use a fork's branch instead of a release because at the time of writing this,
# the wifi support for ESP32 is not yet merged.
# https://github.com/zephyrproject-rtos/zephyr/issues/3723
# ENV ZEPHYR_VERSION 2.4.0
# RUN west init /opt/zephyrproject --mr v${ZEPHYR_VERSION}
RUN git clone https://github.com/mahavirj/zephyr.git --branch feature/esp32_networking_support_on_zephyr_v2.4.0-rc1
RUN west init
RUN west update

ENV ZEPHYR_BASE ${WORKDIR}/zephyr

# Install zephyr dependencies
RUN python3 -m pip install -r ${ZEPHYR_BASE}/scripts/requirements.txt

# Use espressif HAL included in zephyr
ENV ESP_IDF_PATH "/opt/zephyrproject/modules/hal/espressif"

# Install ESP32 hardware abstraction layer (HAL) requirements
RUN python3 -m pip install -r ${WORKDIR}/modules/hal/espressif/requirements.txt
RUN python3 -m pip install gdbgui==0.13.2.0  # This one is not correctly installed on the previous step for some reason

# Reduce image size
RUN rm -rf /var/lib/apt
