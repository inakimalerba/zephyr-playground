# Zephyr Playground

Docker image + scripts to build and flash a Zephyr app the esp32 board as easy as possible.

It contains the ESP32 toolchain and Zephyr OS installed.

## How to use

To enter the image, a dockershell script is provided.
This script configures the necessary stuff so you don't need to do extra work.
The `PWD` is mounted on `/opt/zephyrproject/code` and the workdir is set to `/opt/zephyrproject` so you can call `west` commands straight forward.

### Compiling and Flashing

A hello world project is included in `src/helloworld`.

To build this command, enter the dockershell and execute:

```bash
west build -p auto -b esp32 code/src/helloworld
```

Resulting artifacts are left in `/opt/zephyrproject/build`.


After a successful build, you can flash it to the board.
Make sure it's connected, and run:

```bash
west flash
```
